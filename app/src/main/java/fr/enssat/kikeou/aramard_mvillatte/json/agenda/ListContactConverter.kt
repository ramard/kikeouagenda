package fr.enssat.kikeou.aramard_mvillatte.json.agenda

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import fr.enssat.kikeou.aramard_mvillatte.room.user.Contact

class ListContactConverter {
    private val moshi = Moshi.Builder().build()
    private val contactType = Types.newParameterizedType(List::class.java, Contact::class.java)
    private val contactAdapter = moshi.adapter<List<Contact>>(contactType)

    @TypeConverter
    fun stringToContacts(string: String): List<Contact> {
        return contactAdapter.fromJson(string).orEmpty()
    }

    @TypeConverter
    fun ContactsToString(contacts: List<Contact>): String {
        return contactAdapter.toJson(contacts)
    }

}