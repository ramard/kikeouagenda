package fr.enssat.kikeou.aramard_mvillatte.adapters.external_users

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.aramard_mvillatte.databinding.ExternalUserItemBinding
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda
import fr.enssat.kikeou.aramard_mvillatte.room.user.User

class ExternalUsersListAdapter(): ListAdapter<User, ExternalUsersViewHolder>(UserDiff()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExternalUsersViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            var binding = ExternalUserItemBinding.inflate(inflater, parent, false)
            return ExternalUsersViewHolder.create(binding)
        }

        override fun onBindViewHolder(holder: ExternalUsersViewHolder, position: Int) {
            val user: User = getItem(position)
            val id = user.externalId
            val name = user.name
            val email = user.contacts.firstOrNull { it.key == "email" }?.value ?: ""
            val tel = user.contacts.firstOrNull { it.key == "tel" }?.value ?: ""
            holder.bind(id, name, email, tel)
        }

        internal class UserDiff : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem === newItem
            }
            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
                return oldItem.externalId == newItem.externalId
            }
        }
}