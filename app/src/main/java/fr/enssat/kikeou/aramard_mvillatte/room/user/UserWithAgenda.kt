package fr.enssat.kikeou.aramard_mvillatte.room.user

import androidx.room.Embedded
import androidx.room.Relation
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda

@JsonClass(generateAdapter = true)
data class UserWithAgenda (
    @Embedded
    val user: User,
    @Relation(
        parentColumn = "id",
        entityColumn = "userCreatorId"
    )

    val agendas: List<Agenda>
)