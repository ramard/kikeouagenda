package fr.enssat.kikeou.aramard_mvillatte.room.agenda

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ListLocationConverter
import fr.enssat.kikeou.aramard_mvillatte.room.user.User

@JsonClass(generateAdapter = true)
data class Location(
    var day:Int,
    var value:String
    )

@Entity(tableName = "agenda_table",
    indices = [Index(value = ["userCreatorId", "week"], unique = true)],
    foreignKeys = [
    ForeignKey(
        entity = User::class,
        parentColumns = ["id"],
        childColumns = ["userCreatorId"],
        onDelete = CASCADE
    )
])
@JsonClass(generateAdapter = true)
data class Agenda(
    @ColumnInfo(name = "week")
    @field:Json(name = "week") var week:Int,

    @TypeConverters(ListLocationConverter::class)
    @field:Json(name = "loc") var loc: List<Location>,

    @Transient
    @ColumnInfo(name = "userCreatorId")
    var userCreatorId: Long = 0
    ) {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @Transient
    private var id:Long = 0
        fun getId():Long {
            return id
        }
        fun  setId(value:Long){
            id = value
        }
}