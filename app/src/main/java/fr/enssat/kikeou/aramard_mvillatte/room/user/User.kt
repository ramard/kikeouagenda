package fr.enssat.kikeou.aramard_mvillatte.room.user

import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ListContactConverter
import java.util.*

@JsonClass(generateAdapter = true)
data class Contact(var key: String, var value: String)

@Entity(tableName = "user_table", indices = [Index(value = ["externalId"], unique = true)])
@JsonClass(generateAdapter = true)
data class User(
    @ColumnInfo(name = "name")
    @field:Json(name = "name") var name: String,

    @ColumnInfo(name = "photo")
    @field:Json(name = "photo") var photo: String = "",

    @TypeConverters(ListContactConverter::class)
    @field:Json(name = "contacts") var contacts: List<Contact>,

    @Transient
    @ColumnInfo(name = "phone_owner")
    var phone_owner: Boolean = false,

    @ColumnInfo(name = "externalId")
    @field:Json(name = "id") var externalId: String = UUID.randomUUID().toString()
) {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @Transient
    private var id: Long = 0

    fun getId(): Long {
        return id
    }

    fun setId(value: Long) {
        id = value
    }
}