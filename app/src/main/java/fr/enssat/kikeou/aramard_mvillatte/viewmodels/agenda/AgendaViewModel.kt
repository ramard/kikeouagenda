package fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda

import androidx.annotation.WorkerThread
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import androidx.lifecycle.asLiveData
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ExternalAgenda
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.AgendaRepository
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserWithAgenda
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class AgendaViewModel (private val repository: AgendaRepository) : ViewModel() {

    var week: Int = 1

    fun incrementWeek(){
        week++
        checkWeek()
    }

    fun decrementWeek(){
        week--
        checkWeek()
    }

    fun checkWeek() {
        if(week > 52) {
            week = 1
        }
        if (week < 1) {
            week = 52
        }
    }

    private val scope = CoroutineScope(SupervisorJob())

    @WorkerThread
    fun insertExternalAgenda(agd: Agenda) = scope.launch {
        repository.insert(agd)
    }

    @WorkerThread
    fun deleteAgenda(agd: Agenda) = scope.launch {
        repository.delete(agd)
    }

    @WorkerThread
    fun insertExternalAgenda(externalAgenda: ExternalAgenda) = scope.launch {
        val user = User(
            name = externalAgenda.name,
            photo = externalAgenda.photo,
            contacts = externalAgenda.contacts,
            externalId = externalAgenda.externalId,
            phone_owner = false
        )

        val agenda = Agenda(
            week = externalAgenda.week,
            loc = externalAgenda.loc
        )

        val userWithAgenda = UserWithAgenda(user, agendas = listOf(agenda))

        repository.insert(userWithAgenda)
    }

    fun getAllContactsAgendaFlow(): LiveData<List<Agenda>>  {
        return repository.getAllContactsAgendaByWeekFlow().asLiveData()
    }

    fun getPhoneOwnerAgendaByWeekFlow(week: Int): LiveData<Agenda?> {
        return repository.getPhoneOwnerAgendaByWeekFlow(week).asLiveData()
    }

    fun getExternalUserAgendaByWeekFlow(externalUserID: String, week: Int): LiveData<Agenda?> {
        return repository.getExternalUserAgendaByWeekFlow(externalUserID, week).asLiveData()
    }
}
