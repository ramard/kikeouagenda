package fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.AgendaRepository

class AgendaViewModelFactory(private val repository: AgendaRepository): ViewModelProvider.Factory {
   @Suppress("UNCHECKED_CAST")
    //factory created to pass repository to view model...
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AgendaViewModel(repository) as T
    }
 }
