package fr.enssat.kikeou.aramard_mvillatte.ui.external_user

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import fr.enssat.kikeou.aramard_mvillatte.R
import fr.enssat.kikeou.aramard_mvillatte.ui.agenda.AgendaFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_informations,
    R.string.tab_agenda
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, private val externalUserID: String)
    : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
       val fragment = when(position) {
            0 -> ExternalUserInformationsFragment.newInstance(externalUserID)
            1 -> AgendaFragment.newInstance(externalUserID)
            else -> AgendaFragment.newInstance(externalUserID)
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 2
    }
}