package fr.enssat.kikeou.aramard_mvillatte.adapters.locations

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.aramard_mvillatte.databinding.LocationItemBinding


class LocationViewHolder private constructor(var binding:LocationItemBinding):RecyclerView.ViewHolder(binding.root) {

    fun bind(week: Int, day: String?, value: String?) {
        binding.day.text = day
        binding.value.text = value
    }

    companion object {
        fun create(binding: LocationItemBinding): LocationViewHolder {
            return LocationViewHolder(binding)
        }
    }
}