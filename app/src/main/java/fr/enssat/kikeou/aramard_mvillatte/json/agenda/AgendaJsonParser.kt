package fr.enssat.kikeou.aramard_mvillatte.json.agenda

import android.annotation.SuppressLint
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserWithAgenda

class AgendaJsonParser {

    companion object {
        fun parseAgenda(json: String): Agenda? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Agenda> = moshi.adapter(Agenda::class.java)
            return adapter.fromJson(json)
        }

        fun returnJson(agenda: Agenda): String? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Agenda> = moshi.adapter(Agenda::class.java)
            return adapter.toJson(agenda)
        }

        @SuppressLint("CheckResult")
        fun parseExternalAgenda(json: String): ExternalAgenda? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<ExternalAgenda> = moshi.adapter(ExternalAgenda::class.java)
            return adapter.lenient().fromJson(json)
        }

        fun returnJson(user: User, agenda: Agenda): String? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<ExternalAgenda> = moshi.adapter(ExternalAgenda::class.java)

            val externalId = user.externalId
            val name = user.name
            val contacts = user.contacts
            val photo = user.photo
            val locations = agenda.loc
            val week = agenda.week

            val externalAgenda = ExternalAgenda(
                externalId = externalId,
                name = name,
                contacts = contacts,
                photo = photo,
                loc = locations,
                week = week
            )

            return adapter.toJson(externalAgenda)
        }
    }

}