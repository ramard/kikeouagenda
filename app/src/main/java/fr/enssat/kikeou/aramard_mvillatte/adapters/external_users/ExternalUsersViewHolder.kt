package fr.enssat.kikeou.aramard_mvillatte.adapters.external_users

import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.aramard_mvillatte.databinding.ExternalUserItemBinding


class ExternalUsersViewHolder private constructor(var binding:ExternalUserItemBinding):RecyclerView.ViewHolder(binding.root) {

    fun bind(id: String, name: String, email: String, tel: String) {
        binding.externalUserID.text = id
        binding.username.text = name
        binding.email.text = email
        binding.tel.text = tel
    }

    companion object {
        fun create(binding: ExternalUserItemBinding): ExternalUsersViewHolder {
            return ExternalUsersViewHolder(binding)
        }
    }
}