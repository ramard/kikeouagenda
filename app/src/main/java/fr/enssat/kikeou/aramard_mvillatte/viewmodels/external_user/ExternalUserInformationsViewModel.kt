package fr.enssat.kikeou.aramard_mvillatte.viewmodels.external_user

import androidx.lifecycle.*
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ExternalUserInformationsViewModel(private val repository: UserRepository) : ViewModel() {

    private val scope = CoroutineScope(SupervisorJob())

    fun getExternalUserLiveData(id: String): LiveData<User?> {
        return repository.getExternalUserFlow(id).asLiveData()
    }
}