package fr.enssat.kikeou.aramard_mvillatte.room.agenda

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ListContactConverter
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ListLocationConverter
import fr.enssat.kikeou.aramard_mvillatte.room.user.Contact
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserDao
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserWithAgenda
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@TypeConverters(ListLocationConverter::class, ListContactConverter::class)
@Database(entities = [Agenda::class, User::class], version = 1, exportSchema = false)
abstract class KikeouRoomDatabase : RoomDatabase() {

        abstract fun agendaDao(): AgendaDao
        abstract fun userDao(): UserDao

        companion object {
            @Volatile
            private var INSTANCE: KikeouRoomDatabase? = null

            fun getDatabase(
                context: Context,
                scope: CoroutineScope
            ): KikeouRoomDatabase {
                // if the INSTANCE is not null, then return it,
                // if it is, then create the database
                return INSTANCE ?: synchronized(this) {
                    val aux = Room.databaseBuilder(
                        context.applicationContext,
                        KikeouRoomDatabase::class.java,
                        "kikeou_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(AgendaDatabaseCallback(scope))
                        .build()

                    INSTANCE = aux
                    aux
                }
            }

            private class AgendaDatabaseCallback(val scope: CoroutineScope) : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    INSTANCE?.let { database ->
                        scope.launch(Dispatchers.IO) {
                            populate(database.agendaDao(), database.userDao())
                        }
                    }
                }
            }

            /* populate db in coroutine*/
            fun populate(agendaDao: AgendaDao, userDao: UserDao) {
                agendaDao.deleteAll()
                userDao.deleteAll()
                var user: User
                var locations = listOf<Location>()
                var agendas = listOf<Agenda>()
                var userWithAgenda: UserWithAgenda

                /*
                // User par défaut pour l'application

                user = User(name = "Axel RAMARD", contacts = listOf(Contact("email", "aramard@enssat.fr"), Contact("tel", "06 80 81 82 83")), phone_owner = true)

                locations = listOf(Location(1, "en TT"), Location(2, "en TT"), Location(3, "en présentiel"), Location(4, "en présentiel"), Location(5, "en présentiel"))
                agendas = listOf(Agenda(week = 47, loc = locations))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)

                locations = listOf(Location(1, "en présentiel"), Location(2, "en présentiel"), Location(3, "en présenciel"), Location(4, "en présenciel"), Location(5, "en présenciel"))
                agendas = listOf(Agenda(week = 48, loc = locations))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)

                locations = listOf(Location(1, "en TT"), Location(2, "en TT"), Location(3, "en présenciel"), Location(4, "en présenciel"), Location(5, "en TT"))
                agendas = listOf(Agenda(week = 49, loc = locations))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)

                locations = listOf(Location(1, "en TT"), Location(2, "en TT"), Location(3, "en TT"), Location(4, "en TT"), Location(5, "en présenciel"))
                agendas = listOf(Agenda(week = 50, loc = locations))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)
                */

                // Agendas de 2 contacts
                user = User(name = "Maxime VILLATTE", contacts = listOf(Contact("email", "mvillatt@enssat.fr"), Contact("tel", "06 80 81 82 83")))
                locations = listOf(Location(1, "en vancances"), Location(2, "en vancances"), Location(3, "en vancances"), Location(4, "en vancances"), Location(5, "en vacances"))
                agendas = listOf(Agenda(week = 52, loc = locations))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)

                user = User(name = "Axel RAMARD", contacts = listOf(Contact("email", "aramard@enssat.fr"), Contact("tel", "06 84 85 86 87")))
                userWithAgenda = UserWithAgenda(user, agendas)
                agendaDao.insert(userWithAgenda)
             }
        }
}
