package fr.enssat.kikeou.aramard_mvillatte.room.agenda

import androidx.annotation.WorkerThread
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserWithAgenda
import kotlinx.coroutines.flow.*

class AgendaRepository(private val dao: AgendaDao) {

    // call on a non-UI thread
    @WorkerThread
    suspend fun insert(agenda: Agenda) {
        dao.insert(agenda)
    }

    @WorkerThread
    suspend fun delete(agenda: Agenda) {
        dao.delete(agenda)
    }

    @WorkerThread
    suspend fun insert(userWithAgenda: UserWithAgenda) {
        dao.insert(userWithAgenda)
    }

    fun getAllContactsAgendaByWeekFlow(): Flow<List<Agenda>> {
        return dao.getAllContactsAgendaByWeekFlow()
    }

    fun getPhoneOwnerAgendaByWeekFlow(week: Int): Flow<Agenda?> {
        return dao.getAgendaOfPhoneOwnerByWeekFlow(week)
    }

    fun getExternalUserAgendaByWeekFlow(externalUserID: String, week: Int): Flow<Agenda?> {
        return dao.getExternalUserAgendaByWeekFlow(externalUserID, week)
    }

    @WorkerThread
    suspend fun getPhoneOwnerAgendaByWeek(week: Int): Agenda? {
        return dao.getPhoneOwnerAgendaByWeek(week)
    }

    @WorkerThread
    suspend fun getExternalAgendaByWeek(userExternalID: String, week: Int): Agenda? {
        return dao.getExternalAgendaByWeek(userExternalID, week)
    }

    @WorkerThread
    suspend fun getPhoneOwnerUserWithAgendaByWeek(week: Int): UserWithAgenda? {
        return dao.getPhoneOwnerUserWithAgendaByWeek(week)
    }

    @WorkerThread
    suspend fun getExternalUserWithAgendaByWeek(externalUserID: String, week: Int): UserWithAgenda? {
        return dao.getExternalUserWithAgendaByWeek(externalUserID, week)
    }
}

