package fr.enssat.kikeou.aramard_mvillatte.ui.external_user_list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import fr.enssat.kikeou.aramard_mvillatte.R
import fr.enssat.kikeou.aramard_mvillatte.RecyclerItemClickListener
import fr.enssat.kikeou.aramard_mvillatte.adapters.external_users.ExternalUsersListAdapter
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentExternalUsersListBinding
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.KikeouRoomDatabase
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class ExternalUsersListFragment : Fragment() {

    private var thiscontext: Context? = null

    private lateinit var _userViewModel: UserViewModel
    private lateinit var _recyclerView: RecyclerView

    private lateinit var actionMode: ActionMode
    private lateinit var callback: ActionMode.Callback
    private var isContextualBarDislayed = false

    private var _binding: FragmentExternalUsersListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callback = object : ActionMode.Callback {

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                requireActivity().menuInflater.inflate(R.menu.contextual_action_bar, menu)
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                return when (item?.itemId) {
                    R.id.delete -> {
                        lifecycleScope.launch(Dispatchers.Main) {
                            removeExternalUsersSelected()
                        }
                        true
                    }
                    else -> false
                }
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
                isContextualBarDislayed = false
                uncheckAllExternalUsers()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (container != null) {
            thiscontext = container.context
        }

        _binding = FragmentExternalUsersListBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val adapter = ExternalUsersListAdapter()
        _recyclerView = binding.recyclerview
        _recyclerView.adapter = adapter
        _recyclerView.layoutManager = LinearLayoutManager(thiscontext)

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouRoomDatabase.getDatabase(thiscontext!!, scope)
        val repository = UserRepository(database.userDao())

        // Get a new or existing ViewModel using User view model factory.
        _userViewModel = ViewModelProvider(
            this,
            UserViewModelFactory(repository)
        ).get(UserViewModel::class.java)

        this.activity?.let {
            _userViewModel.getExternalUsersLiveData().observe(it) { users: List<User>? ->
                adapter.submitList(users)
            }
        }

        initializeListeners()

        return root
    }

    /**
     *  Initialisation des listeners sur les éléments intéractifs (boutons, card, ...)
     * */
    private fun initializeListeners() {
        /** Gestion des cliques sur les item du recycler view */
        _recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(context, _recyclerView, object :
                RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int) {
                    val externalUserID = getExternalUserIDAtPos(position)
                    Log.i("[DEBUG]", "Exernal id : $externalUserID")
                    val action = ExternalUsersListFragmentDirections.actionNavigationExternalUsersToExternalUserInformationsFragment(externalUserID)
                    Navigation.findNavController(binding.recyclerview).navigate(action)
                }

                override fun onLongItemClick(view: View?, position: Int) {
                    if (!isContextualBarDislayed) {
                        displayContextualBar()
                    }

                    val checked = isExternalUserChecked(position)
                    checkExternalUser(position, !checked)

                    val nbSelected = getAllSelectedExternalUsers().size

                    if (nbSelected == 0 ) {
                        actionMode.finish()
                    } else {
                        actionMode.title = "$nbSelected sélectionné"
                    }
                }
            })
        )
    }

    private suspend fun removeExternalUsersSelected() {
        val externalUserIDs = getAllSelectedExternalUsers()
        externalUserIDs.forEach { id ->
            val user = _userViewModel.getExternalUserByExternalID(id)
            if (user != null) {
                _userViewModel.deleteUser(user)
            }
        }
        actionMode.finish()
    }

    private fun uncheckAllExternalUsers() {
        _recyclerView.forEach { item ->
            val card: MaterialCardView = item.findViewById(R.id.externalUserItem)
            card.isChecked = false
        }
    }

    private fun getAllSelectedExternalUsers(): List<String> {
        val selectedItems = mutableListOf<String>()
        _recyclerView.forEach { item ->
            val card: MaterialCardView = item.findViewById(R.id.externalUserItem)
            if (card.isChecked) {
                val externalUserID = card.findViewById<TextView>(R.id.externalUserID).text
                selectedItems += externalUserID.toString()
            }
        }
        return selectedItems.toList()

    }

    private fun isExternalUserChecked(position: Int): Boolean {
        val card: MaterialCardView = _recyclerView.getChildAt(position).findViewById(R.id.externalUserItem)
        return card.isChecked
    }

    private fun checkExternalUser(position: Int, check: Boolean) {
        val card: MaterialCardView = _recyclerView.getChildAt(position).findViewById(R.id.externalUserItem)
        card.isChecked = check
    }

    private fun getExternalUserIDAtPos(position: Int): String {
        val card: MaterialCardView = _recyclerView.getChildAt(position).findViewById(R.id.externalUserItem)
        return card.findViewById<TextView>(R.id.externalUserID).text.toString()
    }

    private fun displayContextualBar() {
        isContextualBarDislayed = true
        actionMode = requireActivity().startActionMode(callback)!!
        actionMode.title = "1 sélectionné"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isContextualBarDislayed) actionMode.finish()
        _binding = null
    }
}