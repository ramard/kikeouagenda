package fr.enssat.kikeou.aramard_mvillatte.adapters.locations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.aramard_mvillatte.databinding.LocationItemBinding
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Location

class LocationListAdapter : ListAdapter<Location, LocationViewHolder>(LocationDiff()) {

    var week:Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LocationItemBinding.inflate(inflater, parent, false)
        return LocationViewHolder.create(binding)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val location: Location? = getItem(position)
        val day = when (location?.day) {
            1 -> "Lundi"
            2 -> "Mardi"
            3 -> "Mercredi"
            4 -> "Jeudi"
            5 -> "Vendredi"
            else -> "Unknow"
        }

        val value = location?.value
        holder.bind(week, day, value)
    }

    internal class LocationDiff : DiffUtil.ItemCallback<Location>() {
        override fun areItemsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Location, newItem: Location): Boolean {
            return oldItem.day == newItem.day
        }
    }
}