package fr.enssat.kikeou.aramard_mvillatte.room.user

import androidx.room.*
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda
import kotlinx.coroutines.flow.Flow


@Dao
interface UserDao {
    // allowing the insert of agenda with conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(User: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("DELETE FROM user_table")
    fun deleteAll()

    @Query("SELECT user_table.* FROM user_table where phone_owner = 1")
    fun getPhoneOwnerUserFlow(): Flow<User?>

    @Query("SELECT user_table.* FROM user_table where externalId = :id")
    fun getExternalUserFlow(id: String): Flow<User?>

    @Query("SELECT user_table.* FROM user_table where phone_owner = 1")
    suspend fun getPhoneOwnerUser(): User?

    @Query("SELECT user_table.* FROM user_table where externalId = :externalUserID")
    suspend fun getExternalUser(externalUserID: String): User

    @Query("SELECT user_table.* FROM user_table where phone_owner = 0")
    fun getExternalUsersFlow(): Flow<List<User>?>

}

