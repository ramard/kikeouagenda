package fr.enssat.kikeou.aramard_mvillatte.ui.userinformations

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.aramard_mvillatte.Utility.Companion.hideKeyboard
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentUserinformationsBinding
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.KikeouRoomDatabase
import fr.enssat.kikeou.aramard_mvillatte.room.user.Contact
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class UserInformationsFragment : Fragment() {

    private var thiscontext: Context? = null

    private lateinit var _userViewModel: UserViewModel
    private lateinit var _binding: FragmentUserinformationsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (container != null) {
            thiscontext = container.context
        }

        _binding = FragmentUserinformationsBinding.inflate(inflater, container, false)
        val root: View = _binding.root

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouRoomDatabase.getDatabase(thiscontext!!, scope)
        val repository = UserRepository(database.userDao())

        // Get a new or existing ViewModel using Agenda view model factory.
        _userViewModel = ViewModelProvider(
            this,
            UserViewModelFactory(repository)
        ).get(UserViewModel::class.java)

        _userViewModel.getPhoneOwnerUserLiveData().observe(this.requireActivity()) { user: User? ->
            if (user != null) {
                _binding.name.setText(user.name)
                val email = user.contacts.firstOrNull { it.key == "email" }
                val tel = user.contacts.firstOrNull { it.key == "tel" }
                _binding.email.setText(email?.value)
                _binding.tel.setText(tel?.value)
            }
        }

        val addEvent = _binding.btnValidateInformations
        addEvent.setOnClickListener {
            val email = _binding.email.text.toString()
            val tel = _binding.tel.text.toString()
            val name = _binding.name.text.toString()

            val user = User(name = name, contacts = listOf(Contact("email", email), Contact("tel", tel)), phone_owner = true)
            _userViewModel.insertOrUpdatePhoneOwnerUser(user)
            Toast.makeText(context, "Informations enregistrées", Toast.LENGTH_SHORT).show()
            hideKeyboard()
        }

        return root
    }
}