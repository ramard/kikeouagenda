package fr.enssat.kikeou.aramard_mvillatte.json.agenda

import androidx.room.ColumnInfo
import androidx.room.TypeConverters
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.JsonQualifier
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Location
import fr.enssat.kikeou.aramard_mvillatte.room.user.Contact

@JsonClass(generateAdapter = true)
data class ExternalAgenda(
    @field:Json(name = "id") var externalId: String,

    @field:Json(name = "name") var name: String,

    @field:Json(name = "photo") var photo: String = "",

    @TypeConverters(ListContactConverter::class)
    @field:Json(name = "contact") var contacts: List<Contact>,

    @field:Json(name = "week") var week:Int,

    @TypeConverters(ListLocationConverter::class)
    @field:Json(name = "loc") var loc: List<Location>,

)
