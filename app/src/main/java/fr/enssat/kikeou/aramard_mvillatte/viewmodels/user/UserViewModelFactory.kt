package fr.enssat.kikeou.aramard_mvillatte.viewmodels.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.AgendaRepository
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository

class UserViewModelFactory(private val repository: UserRepository): ViewModelProvider.Factory {
   @Suppress("UNCHECKED_CAST")
    //factory created to pass repository to view model...
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UserViewModel(repository) as T
    }
 }
