package fr.enssat.kikeou.aramard_mvillatte.ui.external_user

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentExternalUserInformationsBinding
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.KikeouRoomDatabase
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.external_user.ExternalUserInformationsViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.external_user.ExternalUserInformationsViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob


/**
 * A placeholder fragment containing a simple view.
 */
class ExternalUserInformationsFragment : Fragment() {

    private lateinit var externalUserID: String


    private lateinit var externalUserInformationsViewModel: ExternalUserInformationsViewModel
    private var _binding: FragmentExternalUserInformationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            externalUserID = it.getString(EXTERNAL_USER_ID).toString()
        }
        Log.i("[DEBUG]", "L'external id du user sélectionné est : $externalUserID")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentExternalUserInformationsBinding.inflate(inflater, container, false)
        val root = binding.root

        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouRoomDatabase.getDatabase(requireContext(), scope)
        val repository = UserRepository(database.userDao())

        // Get a new or existing ViewModel using Agenda view model factory.
        externalUserInformationsViewModel = ViewModelProvider(
            this,
            ExternalUserInformationsViewModelFactory(repository)
        ).get(ExternalUserInformationsViewModel::class.java)

        externalUserInformationsViewModel.getExternalUserLiveData(externalUserID).observe(this.requireActivity()) { user: User? ->
            if (user != null) {
                binding.username.text = user.name
                val email = user.contacts.firstOrNull { it.key == "email" }
                val tel = user.contacts.firstOrNull { it.key == "tel" }
                binding.email.text = email?.value
                binding.tel.text = tel?.value
            }
        }


        return root
    }

    companion object {
        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(externalUserID: String): ExternalUserInformationsFragment {
            return ExternalUserInformationsFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTERNAL_USER_ID, externalUserID)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}