package fr.enssat.kikeou.aramard_mvillatte.room.user

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class UserRepository(private val dao: UserDao) {
    // call on a non-UI thread
    @WorkerThread
    suspend fun insertOrUpdatePhoneOwnerUser(user: User) {
        val userInBase = dao.getPhoneOwnerUser()
        if (userInBase != null) {
            userInBase.name = user.name
            userInBase.contacts = user.contacts
            dao.update(userInBase)
        } else {
            dao.insert(user)
        }
    }

    @WorkerThread
    suspend fun delete(user: User) {
        dao.delete(user)
    }

    @WorkerThread
    suspend fun update(user: User) {
        dao.update(user)
    }

    fun getPhoneOwnerUserFlow(): Flow<User?> {
       return dao.getPhoneOwnerUserFlow()
    }

    fun getExternalUserFlow(id: String): Flow<User?> {
       return dao.getExternalUserFlow(id)
    }

    @WorkerThread
    suspend fun getPhoneOwnerUser() : User? {
        return dao.getPhoneOwnerUser()
    }

    @WorkerThread
    suspend fun getExternalUser(externalUserID: String) : User? {
        return dao.getExternalUser(externalUserID)
    }

    fun getExternalUsersFlow(): Flow<List<User>?> {
        return dao.getExternalUsersFlow()
    }
}

