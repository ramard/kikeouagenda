package fr.enssat.kikeou.aramard_mvillatte.viewmodels.user

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import androidx.lifecycle.asLiveData
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class UserViewModel (private val repository: UserRepository) : ViewModel() {

    private val scope = CoroutineScope(SupervisorJob())

    /**
     * insert the data in non-blocking coroutine
     */
    fun insertOrUpdatePhoneOwnerUser(user: User) = scope.launch {
        repository.insertOrUpdatePhoneOwnerUser(user)
    }

    fun deleteUser(user: User) = scope.launch {
        repository.delete(user)
    }

    fun update(user: User) = scope.launch {
        repository.update(user)
    }

    fun getPhoneOwnerUserLiveData(): LiveData<User?> {
        return repository.getPhoneOwnerUserFlow().asLiveData()
    }

    suspend fun getExternalUserByExternalID(externalUserID: String): User? {
        return repository.getExternalUser(externalUserID)
    }

    fun getExternalUsersLiveData(): LiveData<List<User>?> {
        return repository.getExternalUsersFlow().asLiveData()
    }
}
