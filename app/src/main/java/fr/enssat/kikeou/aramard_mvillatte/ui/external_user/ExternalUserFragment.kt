package fr.enssat.kikeou.aramard_mvillatte.ui.external_user

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentExternalUserBinding

const val EXTERNAL_USER_ID = "external_user_id"


class ExternalUserFragment : Fragment() {
    private lateinit var externalUserID: String
    private var _binding: FragmentExternalUserBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            externalUserID = it.getString(EXTERNAL_USER_ID).toString()
        }
        Log.i("[DEBUG]", "L'external id du user sélectionné est : $externalUserID")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentExternalUserBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val sectionsPagerAdapter = SectionsPagerAdapter(
            requireContext(),
            requireActivity().supportFragmentManager,
            externalUserID
        )
        val viewPager: ViewPager = binding.viewPager
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = binding.tabs
        tabs.setupWithViewPager(viewPager)

        return root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param externalUserID Id externe de l'utilisateur à afficher.
         * @return A new instance of fragment UserInformationsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(externalUserID: String) =
            ExternalUserInformationsFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTERNAL_USER_ID, externalUserID)
                }
            }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}