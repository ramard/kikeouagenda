package fr.enssat.kikeou.aramard_mvillatte.room.agenda

import androidx.room.*
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserWithAgenda
import kotlinx.coroutines.flow.Flow

@Dao
interface AgendaDao {
    // allowing the insert of agenda with conflict resolution strategy

    @Transaction
    fun insert(userWithAgenda: UserWithAgenda) {
        val user = userWithAgenda.user
        val agendas = userWithAgenda.agendas
        // Save rowId of inserted User as userId
        val userId: Long = insert(user)

        // Set userId for all related agendas
        for (agenda in agendas) {
            agenda.userCreatorId = userId
            insert(agenda)
        }

    }

    @Query("DELETE FROM agenda_table")
    fun deleteAll()

    @Transaction
    @Query("SELECT agenda_table.* FROM agenda_table JOIN user_table as user on userCreatorId = user.id where phone_owner = 0")
    fun getAllContactsAgendaByWeekFlow(): Flow<List<Agenda>>

    @Transaction
    @Query("SELECT agenda_table.* FROM agenda_table JOIN user_table as user on userCreatorId = user.id where week = :week and phone_owner = 1")
    fun getAgendaOfPhoneOwnerByWeekFlow(week: Int): Flow<Agenda?>

    @Transaction
    @Query("SELECT agenda_table.* FROM agenda_table JOIN user_table as user on userCreatorId = user.id where week = :week and externalId = :userExternalId")
    fun getExternalUserAgendaByWeekFlow(userExternalId: String, week: Int): Flow<Agenda?>

    @Transaction
    @Query("SELECT agenda_table.* FROM agenda_table JOIN user_table as user on userCreatorId = user.id where week = :week and phone_owner = 1")
    suspend fun getPhoneOwnerAgendaByWeek(week: Int): Agenda?

    @Transaction
    @Query("SELECT agenda_table.* FROM agenda_table JOIN user_table as user on userCreatorId = user.id where user.externalId = :externalUserID and week = :week")
    suspend fun getExternalAgendaByWeek(externalUserID: String, week: Int): Agenda?

    @Transaction
    @Query("SELECT * FROM agenda_table AS agenda JOIN user_table AS user ON userCreatorId = user.id WHERE (phone_owner = 1) AND (agenda.week = :week)")
    suspend fun getPhoneOwnerUserWithAgendaByWeek(week: Int): UserWithAgenda?

    @Transaction
    @Query("SELECT * FROM agenda_table JOIN user_table as user on userCreatorId = user.id where week = :week and user.externalId = :userExternalId")
    suspend fun getExternalUserWithAgendaByWeek(userExternalId: String, week: Int): UserWithAgenda?

    // des sécurités ont été mises en place pour ne pas à avoir à remplacer mais par précaution on préfère écraser les informations du user
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(agenda: Agenda)

    @Delete
    fun delete(agenda: Agenda)
}

