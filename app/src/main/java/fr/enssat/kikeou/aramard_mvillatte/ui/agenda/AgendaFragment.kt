package fr.enssat.kikeou.aramard_mvillatte.ui.agenda

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.aramard_mvillatte.R
import fr.enssat.kikeou.aramard_mvillatte.RecyclerItemClickListener
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentAgendaBinding
import fr.enssat.kikeou.aramard_mvillatte.adapters.locations.LocationListAdapter
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.AgendaJsonParser
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.*
import fr.enssat.kikeou.aramard_mvillatte.room.user.User
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import fr.enssat.kikeou.aramard_mvillatte.ui.external_user.EXTERNAL_USER_ID
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda.AgendaViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda.AgendaViewModelFactory
import kotlinx.coroutines.*
import net.glxn.qrgen.android.QRCode
import java.util.*
import androidx.core.view.forEach
import com.google.android.material.card.MaterialCardView


class AgendaFragment : Fragment() {
    private var thiscontext: Context? = null

    private lateinit var _agendaViewModel: AgendaViewModel
    private lateinit var _agendaRepository: AgendaRepository
    private lateinit var _userRepository: UserRepository
    private lateinit var _recyclerView: RecyclerView

    private lateinit var actionMode: ActionMode
    private lateinit var callback: ActionMode.Callback
    private var isContextualBarDislayed = false

    private var _binding: FragmentAgendaBinding? = null
    private val locationAdaptater = LocationListAdapter()

    private var externalUserID: String? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            externalUserID = it.getString(EXTERNAL_USER_ID).toString()
        }

        requireActivity().actionBar?.show()

        callback = object : ActionMode.Callback {

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                requireActivity().menuInflater.inflate(R.menu.contextual_action_bar, menu)
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                return when (item?.itemId) {
                    R.id.delete -> {
                        lifecycleScope.launch(Dispatchers.Main) {
                            removeLocationsSelected()
                        }
                        true
                    }
                    else -> false
                }
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
                isContextualBarDislayed = false
                uncheckAllLocations()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.bottom_nav_menu, menu)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (container != null) {
            thiscontext = container.context
        }

        _binding = FragmentAgendaBinding.inflate(inflater, container, false)
        val root: View = binding.root

        _recyclerView = binding.recyclerview
        _recyclerView.adapter = locationAdaptater
        _recyclerView.layoutManager = LinearLayoutManager(thiscontext)

        val scope = CoroutineScope(SupervisorJob())

        /* On récupère les infos sur l'utilsateur */
        val database = KikeouRoomDatabase.getDatabase(thiscontext!!, scope)
        _userRepository = UserRepository(database.userDao())

        /* On récupère les infos sur l'agenda */
        _agendaRepository = AgendaRepository(database.agendaDao())

        _agendaViewModel = ViewModelProvider(
            this,
            AgendaViewModelFactory(_agendaRepository)
        ).get(AgendaViewModel::class.java)

        val week = Calendar.getInstance(TimeZone.getTimeZone("UTC")).get(Calendar.WEEK_OF_YEAR)
        _agendaViewModel.week = week
        locationAdaptater.week = week

        /* On update les éléments visuels lié à la semaine affichée */
        updateWeekView()
        updateLocationsView()

        initializeListeners()


        return root
    }

    /**
     *  Initialisation des listeners sur les éléments intéractifs (boutons, card, ...)
     * */
    private fun initializeListeners() {

        /** Ajout d'une nouvelle location dans la liste */
        val addLocation = binding.btnAddLocation
        addLocation.setOnClickListener {
            if (isContextualBarDislayed) actionMode.finish()
            displayPopupNewLocation()
        }

        /** Partage de l'agenda qui se trouve à l'écran */
        val shareBtn = binding.btnShareCalendar
        shareBtn.setOnClickListener {
            if (isContextualBarDislayed) actionMode.finish()
            lifecycleScope.launch(Dispatchers.Main) { generateQRCode() }
        }

        /** Passage à l'agenda de la semaine suivante */
        val nextWeekBtn = binding.next
        nextWeekBtn.setOnClickListener {
            if (isContextualBarDislayed) actionMode.finish()
            _agendaViewModel.incrementWeek()
            locationAdaptater.week = _agendaViewModel.week
            updateWeekView()
            updateLocationsView()
        }

        /** Passage à l'agenda de la semaine précédente */
        val previousWeekBtn = binding.previous
        previousWeekBtn.setOnClickListener {
            if (isContextualBarDislayed) actionMode.finish()
            _agendaViewModel.decrementWeek()
            locationAdaptater.week = _agendaViewModel.week
            updateWeekView()
            updateLocationsView()
        }

        /** Gestion des cliques sur les item du recycler view */
        _recyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(context, _recyclerView, object :
                RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int) {
                    val card: MaterialCardView =
                        _recyclerView.getChildAt(position).findViewById(R.id.locationItem)
                    val day =
                        getDayNumberByString(card.findViewById<TextView>(R.id.day).text.toString())
                    val description = card.findViewById<TextView>(R.id.value).text.toString()
                    displayPopupNewLocation(day, description)
                }

                override fun onLongItemClick(view: View?, position: Int) {
                    if (!isContextualBarDislayed) {
                        displayContextualBar()
                    }

                    val checked = isLocationChecked(position)
                    checkLocation(position, !checked)

                    val nbSelected = getAllSelectedLocations().size

                    if (nbSelected == 0) {
                        actionMode.finish()
                    } else {
                        actionMode.title = "$nbSelected sélectionné"
                    }
                }
            })
        )
    }

    private fun displayPopupNewLocation(day: Int? = null, text: String? = null) {
        val dialog = Dialog(thiscontext!!)
        dialog.setContentView(R.layout.new_location_popup)
        dialog.window!!.setBackgroundDrawableResource(R.drawable.bg_window)

        val dayPicker = dialog.findViewById<NumberPicker>(R.id.daypicker)
        val locationText = dialog.findViewById<EditText>(R.id.location)

        val days = arrayOf("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi")

        dayPicker.minValue = 1
        dayPicker.maxValue = days.size

        dayPicker.displayedValues = days

        if (day != null) {
            dayPicker.value = day
        }

        if (text != null) {
            locationText.setText(text)
        }

        val btnClose = dialog.findViewById<ImageView>(R.id.btn_close)
        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        val btnValidate = dialog.findViewById<Button>(R.id.btn_validate_location)
        btnValidate.setOnClickListener {
            lifecycleScope.launch(Dispatchers.Main) {
                val location = Location(dayPicker.value, locationText.text.toString())

                /** Si l'on avait founit un jour c'est que l'on modifie une location existante sinon en créé une nouvelle */
                if (day != null) {
                    modifyLocation(day, location)
                } else {
                    addLocation(location)
                }
            }
            dialog.dismiss()
        }

        dialog.show()
    }

    private suspend fun modifyLocation(oldDay: Int, location: Location) {
        val weekSelected = Integer.parseInt(binding.week.text as String)
        val user: User?
        val existingAgenda: Agenda?

        if (externalUserID !== null) {
            user = _userRepository.getExternalUser(externalUserID!!)
            existingAgenda =
                _agendaRepository.getExternalAgendaByWeek(externalUserID!!, weekSelected)
        } else {
            user = _userRepository.getPhoneOwnerUser()
            existingAgenda = _agendaRepository.getPhoneOwnerAgendaByWeek(weekSelected)
        }

        val toastMessage: String
        if (user != null) {
            if (existingAgenda != null) {
                val existingNewLocation = existingAgenda.loc.firstOrNull { it.day == location.day }

                // On empêche d'écraser une location sauf s'il s'agit de la même location (c'est qu'on a juste modifié le texte)
                if (existingNewLocation == null || existingNewLocation.day == oldDay) {
                    // on supprime l'ancienne location
                    val locations = existingAgenda.loc.filter { it.day != oldDay }.toMutableList()

                    // on ajoute la nouvelle location
                    locations += location

                    // on enregistre l'agenda avec les modifications faites sur les locations
                    existingAgenda.loc = locations.toList()
                    _agendaViewModel.insertExternalAgenda(existingAgenda)

                    toastMessage = "Location modifiée"
                } else {
                    toastMessage = "Cette location existe déjà"
                }
            } else {
                toastMessage = "Un problème s'est produit. Impossible de modifier la location"
            }
        } else {
            toastMessage = "Un problème s'est produit. Impossible de modifier la location"
        }
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show()
    }

    private suspend fun addLocation(location: Location) {
        val weekSelected = Integer.parseInt(binding.week.text as String)
        val user: User?
        val existingAgenda: Agenda?
        if (externalUserID !== null) {
            user = _userRepository.getExternalUser(externalUserID!!)
            existingAgenda =
                _agendaRepository.getExternalAgendaByWeek(externalUserID!!, weekSelected)
        } else {
            user = _userRepository.getPhoneOwnerUser()
            existingAgenda = _agendaRepository.getPhoneOwnerAgendaByWeek(weekSelected)
        }

        val toastMessage: String
        if (user != null) {
            if (existingAgenda != null) {
                val existingLocation = existingAgenda.loc.firstOrNull { it.day == location.day }

                if (existingLocation == null) {
                    existingAgenda.loc += location
                    _agendaViewModel.insertExternalAgenda(existingAgenda)
                    toastMessage = "Location ajoutée à la semaine"
                } else {
                    toastMessage = "Location déjà existante"
                }
            } else {
                val newAgenda = Agenda(
                    week = weekSelected,
                    loc = listOf(location),
                    userCreatorId = user.getId()
                )
                newAgenda.let {
                    _agendaViewModel.insertExternalAgenda(it)
                }
                toastMessage = "Location ajoutée à la semaine"
            }

        } else if (externalUserID == null) {
            toastMessage =
                "Veuillez remplir votre profil avant d'ajouter des locations votre agenda"
        } else {
            toastMessage = "Un problème s'est produit. Impossible d'ajouter du contenu à l'agenda"
        }
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show()
    }

    private fun displayPopupQRCode(bitmap: Bitmap?) {
        val dialog = Dialog(thiscontext!!)
        dialog.setContentView(R.layout.share_popup)
        dialog.window!!.setBackgroundDrawableResource(R.drawable.bg_window)

        val qrCodeView = dialog.findViewById<ImageView>(R.id.QRCodeImagePreview)
        qrCodeView.setImageBitmap(bitmap)

        val btnClose = dialog.findViewById<ImageView>(R.id.btn_close)
        btnClose.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private suspend fun generateQRCode() {
        val weekSelected = Integer.parseInt(binding.week.text as String)
        val user: User?
        val agenda: Agenda?

        if (externalUserID == null) {
            user = _userRepository.getPhoneOwnerUser()
            agenda = _agendaRepository.getPhoneOwnerAgendaByWeek(weekSelected)
        } else {
            user = _userRepository.getExternalUser(externalUserID!!)
            agenda = _agendaRepository.getExternalAgendaByWeek(externalUserID!!, weekSelected)
        }

        if (user != null && agenda != null) {
            val json = AgendaJsonParser.returnJson(user, agenda)
            val bitmap = QRCode.from(json).withCharset("UTF-8").withSize(800, 800).bitmap()
            displayPopupQRCode(bitmap)
        } else if (user == null) {
            Toast.makeText(context, "Problème avec le user", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "L'agenda de la semaine est vide", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateWeekView() {
        _binding?.week!!.text = _agendaViewModel.week.toString()
    }

    private fun updateLocationsView() {
        val weekSelected = Integer.parseInt(binding.week.text as String)
        // Agenda de l'utilisateur
        if (externalUserID == null) {
            _agendaViewModel.getPhoneOwnerAgendaByWeekFlow(weekSelected)
                .observe(this.requireActivity()) { agenda: Agenda? ->
                    val locations = agenda?.loc
                    val sortedLocations = locations?.sortedBy { it.day }
                    locationAdaptater.submitList(sortedLocations)
                }
        }
        // Agenda d'un contact externe
        else {
            Log.i("[debug]", "Il s'agit d'un agenda externe")
            _agendaViewModel.getExternalUserAgendaByWeekFlow(externalUserID!!, weekSelected)
                .observe(this.requireActivity()) { agenda: Agenda? ->
                    val locations = agenda?.loc
                    val sortedLocations = locations?.sortedBy { it.day }
                    locationAdaptater.submitList(sortedLocations)
                }
        }
    }

    private fun getAllSelectedLocations(): List<Int> {
        val selectedItems = mutableListOf<Int>()
        _recyclerView.forEach { item ->
            val card: MaterialCardView = item.findViewById(R.id.locationItem)
            if (card.isChecked) {
                val day =
                    getDayNumberByString(card.findViewById<TextView>(R.id.day).text.toString())
                selectedItems += day
            }
        }
        return selectedItems.toList()

    }

    private fun getDayNumberByString(value: String): Int {
        val dayNumber = when (value) {
            "Lundi" -> 1
            "Mardi" -> 2
            "Mercredi" -> 3
            "Jeudi" -> 4
            "Vendredi" -> 5
            else -> -1
        }
        return dayNumber
    }

    private fun uncheckAllLocations() {
        _recyclerView.forEach { item ->
            val card: MaterialCardView = item.findViewById(R.id.locationItem)
            card.isChecked = false
        }
    }

    private fun isLocationChecked(position: Int): Boolean {
        val card: MaterialCardView =
            _recyclerView.getChildAt(position).findViewById(R.id.locationItem)
        return card.isChecked
    }

    private fun checkLocation(position: Int, check: Boolean) {
        val card: MaterialCardView =
            _recyclerView.getChildAt(position).findViewById(R.id.locationItem)
        card.isChecked = check
    }

    private suspend fun removeLocationsSelected() {
        val selectedLocations = getAllSelectedLocations()
        val weekSelected = Integer.parseInt(binding.week.text as String)
        val user: User?
        val existingAgenda: Agenda?

        if (externalUserID !== null) {
            user = _userRepository.getExternalUser(externalUserID!!)
            existingAgenda =
                _agendaRepository.getExternalAgendaByWeek(externalUserID!!, weekSelected)
        } else {
            user = _userRepository.getPhoneOwnerUser()
            existingAgenda = _agendaRepository.getPhoneOwnerAgendaByWeek(weekSelected)
        }

        if (user != null) {
            if (existingAgenda != null) {
                val actualLocations = existingAgenda.loc as MutableList<Location>
                actualLocations.removeAll {
                    it.day in selectedLocations
                }

                /** S'il n'y a plus de locations dans l'agenda on le supprime sinon l'update avec la nouvelle liste de locations */
                if (actualLocations.isNullOrEmpty()) {
                    _agendaViewModel.deleteAgenda(existingAgenda)
                } else {
                    existingAgenda.loc = actualLocations
                    _agendaViewModel.insertExternalAgenda(existingAgenda)
                }

                /** On ferme la bar contextuelle */
                actionMode.finish()
            }
        }
    }

    private fun displayContextualBar() {
        isContextualBarDislayed = true
        actionMode = requireActivity().startActionMode(callback)!!
        actionMode.title = "1 sélectionné"
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param externalUserID Parameter 1.
         * @return A new instance of fragment UserInformationsFragment.
         */
        @JvmStatic
        fun newInstance(externalUserID: String) =
            AgendaFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTERNAL_USER_ID, externalUserID)
                }
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isContextualBarDislayed) actionMode.finish()
        _binding = null
    }
}