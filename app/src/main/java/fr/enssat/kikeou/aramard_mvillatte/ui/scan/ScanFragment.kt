package fr.enssat.kikeou.aramard_mvillatte.ui.scan

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import fr.enssat.kikeou.aramard_mvillatte.databinding.FragmentScanBinding
import java.util.concurrent.ExecutionException
import fr.enssat.kikeou.aramard_mvillatte.R

import android.widget.ImageView

import android.app.Dialog
import android.content.Context
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.AgendaJsonParser
import fr.enssat.kikeou.aramard_mvillatte.json.agenda.ExternalAgenda
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.Agenda
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.AgendaRepository
import fr.enssat.kikeou.aramard_mvillatte.room.agenda.KikeouRoomDatabase
import fr.enssat.kikeou.aramard_mvillatte.room.user.UserRepository
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda.AgendaViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.agenda.AgendaViewModelFactory
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModel
import fr.enssat.kikeou.aramard_mvillatte.viewmodels.user.UserViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.lang.Exception


class ScanFragment : Fragment() {

    private var thiscontext: Context? = null
    private var _binding: FragmentScanBinding? = null

    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var analyzer: MyImageAnalyzer
    private val REQUEST_CODE = 123

    private var isDialogOpen = false

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var _userRepository: UserRepository
    private lateinit var _agendaRepository: AgendaRepository
    private lateinit var _agendaViewModel: AgendaViewModel
    private lateinit var _userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (container != null) {
            thiscontext = container.context
        }

        _binding = FragmentScanBinding.inflate(inflater, container, false)


        val scope = CoroutineScope(SupervisorJob())
        val database = KikeouRoomDatabase.getDatabase(thiscontext!!, scope)
        _agendaRepository = AgendaRepository(database.agendaDao())
        _userRepository = UserRepository(database.userDao())

        // Get a new or existing ViewModel using Agenda view model factory.
        _agendaViewModel = ViewModelProvider(
            this,
            AgendaViewModelFactory(_agendaRepository)
        ).get(AgendaViewModel::class.java)

        _userViewModel = ViewModelProvider(
            this,
            UserViewModelFactory(_userRepository)
        ).get(UserViewModel::class.java)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onResume() {
        super.onResume()
        //Do not forget to declare  <uses-permission android:name="android.permission.CAMERA"/> in your manifest
        if (this.context?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                )
            } == PackageManager.PERMISSION_GRANTED) {
            initCamera()
        } else {
            val permissions = arrayOf(Manifest.permission.CAMERA)
            this.activity?.let { ActivityCompat.requestPermissions(it, permissions, REQUEST_CODE) }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(context, "Permission ok for our app", Toast.LENGTH_LONG).show()
                    initCamera()
                } else {
                    Toast.makeText(context, "Permission denied for our app", Toast.LENGTH_LONG)
                        .show()
                }
                return
            }
        }
    }

    private fun initCamera() = binding.previewview.post {
        //this.window.setFlags(1024, 1024)

        analyzer = MyImageAnalyzer()

        cameraProviderFuture =
            this.context?.let { ProcessCameraProvider.getInstance(it) } as ListenableFuture<ProcessCameraProvider>
        cameraProviderFuture.addListener({
            try {
                if (this.activity?.let {
                        ActivityCompat.checkSelfPermission(
                            it,
                            Manifest.permission.CAMERA
                        )
                    } != PackageManager.PERMISSION_GRANTED) {
                    this.activity?.let {
                        ActivityCompat.requestPermissions(
                            it,
                            arrayOf(Manifest.permission.CAMERA),
                            REQUEST_CODE
                        )
                    }
                } else {
                    val processCameraProvider = cameraProviderFuture.get()
                    bindPreview(processCameraProvider)
                }
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } catch (e: ExecutionException) {
                e.printStackTrace()
            }
        }, ContextCompat.getMainExecutor(this.context))
    }

    private fun bindPreview(processCameraProvider: ProcessCameraProvider?) {
        val preview = Preview.Builder().build().also {
            it.setSurfaceProvider(binding.previewview.surfaceProvider)
        }

        val imageAnalysis = ImageAnalysis.Builder()
            .setTargetResolution(Size(1280, 720))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()

        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(context), analyzer)

        val cameraSelector =
            CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()

        ImageCapture.Builder().build()

        processCameraProvider!!.unbindAll()

        processCameraProvider.bindToLifecycle(
            this as LifecycleOwner,
            cameraSelector,
            preview,
            imageAnalysis
        )

        preview.setSurfaceProvider(binding.previewview.surfaceProvider)
    }

    private inner class MyImageAnalyzer :
        ImageAnalysis.Analyzer {

        //private val bd: bottom_dialog = bottom_dialog() // mettre une popup ici
        override fun analyze(image: ImageProxy) {
            scanbarcode(image)
        }


        @SuppressLint("UnsafeOptInUsageError")
        private fun scanbarcode(imageProxy: ImageProxy) {
            val rotationDegrees = imageProxy.imageInfo.rotationDegrees
            val image = imageProxy.image
            if (image != null) {
                val processImage = InputImage.fromMediaImage(image, rotationDegrees)

                val barcodeScannerOptions = BarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                    .build()

                val barcodeScanner: BarcodeScanner =
                    BarcodeScanning.getClient(barcodeScannerOptions)

                barcodeScanner.process(processImage).addOnSuccessListener {
                    barcodes -> readerBarcodeData(barcodes)
                    imageProxy.close()
                }.addOnFailureListener {
                    Log.e("ScannerActivity", "Error: $it.message")
                    imageProxy.close()
                }
            }
        }

        private fun readerBarcodeData(barcodes: List<Barcode>) {
            for (barcode in barcodes) {
                val rawValue = barcode.rawValue

                when (barcode.valueType) {
                    Barcode.TYPE_TEXT -> {
                        if (rawValue != null && rawValue != "") {
                            var externalAgenda: ExternalAgenda?
                            try {
                                externalAgenda = AgendaJsonParser.parseExternalAgenda(rawValue)
                                if (externalAgenda != null) {
                                    showDialog(externalAgenda)
                                }
                            }catch (e: Exception){
                                Toast.makeText(context, "Impossible de récupérer l'agenda", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showDialog(externalAgenda: ExternalAgenda) {
        // On  regarde si on a pas déjà ouvert une fenêtre popup pour ne pas en ouvrir une dizaine à chaque scan
        if (!isDialogOpen) {
            isDialogOpen = true

            val dialog = this.context?.let { Dialog(it) }
            dialog?.setContentView(R.layout.scan_popup)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.bg_window)

            val btnClose = dialog.findViewById<ImageView>(R.id.btn_close)
            val contactNameView = dialog.findViewById<TextView>(R.id.contactName)
            val btnNo = dialog.findViewById<TextView>(R.id.btn_no)
            val btnYes = dialog.findViewById<TextView>(R.id.btn_yes)

            contactNameView.text = externalAgenda.name

            btnClose.setOnClickListener {
                dialog.dismiss()
                isDialogOpen = false
            }
            btnNo.setOnClickListener {
                dialog.dismiss()
                isDialogOpen = false
            }
            btnYes.setOnClickListener {
                lifecycleScope.launch(Dispatchers.Main) {
                    addExternalUser(externalAgenda)
                }
                dialog.dismiss()
                isDialogOpen = false
            }

            dialog.show()
        }
    }

    private suspend fun addExternalUser(externalAgenda: ExternalAgenda) {
        val existingUser = _userRepository.getExternalUser(externalAgenda.externalId)

        if (existingUser != null) {
            if (!existingUser.phone_owner) {
                // on met à jour les informations de l'utilisateur
                existingUser.name = externalAgenda.name
                existingUser.photo = externalAgenda.photo
                existingUser.contacts = externalAgenda.contacts

                val agenda = Agenda(
                    week = externalAgenda.week,
                    loc = externalAgenda.loc,
                    userCreatorId = existingUser.getId()
                )
                _userViewModel.update(existingUser)
                _agendaViewModel.insertExternalAgenda(agenda)
            } else {
                Toast.makeText(context, "Il s'agit de votre agenda", Toast.LENGTH_LONG).show()
            }
        } else {
            _agendaViewModel.insertExternalAgenda(externalAgenda)
        }

        Toast.makeText(context, "Contact ajouté à votre liste", Toast.LENGTH_LONG).show()
    }
}